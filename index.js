require('dotenv').config();
const express = require('express');
const app = express();
const server = require('http').Server(app);
const bodyParser = require('body-parser');
const routes = require('./routing/routes');
var admin = require('firebase-admin');
var firebase = require('firebase')
var serviceAccount = require("./lib/octo-work-firebase-adminsdk-poad8-394fe4f0dd.json");

const cors = require('cors');
const helmet = require('helmet');

const firebaseConfig = {
    apiKey:process.env.F_apiKey,
    authDomain:process.env.F_authDomain,
    databaseURL:process.env.F_databaseURL,
    projectId:process.env.F_projectId,
    storageBucket:process.env.F_storageBucket,
    messagingSenderId:process.env.F_messagingSenderId,
    appId:process.env.F_appId,
    measurementId:process.env.F_measurementId,
    credential: admin.credential.cert(serviceAccount)
    // apiKey: "AIzaSyBaSs_6KgRKyBa8weoz_X4ezu9VrPsjPUw",
    // authDomain: "admos-enterprise.firebaseapp.com",
    // databaseURL: "https://admos-enterprise.firebaseio.com",
    // projectId: "admos-enterprise",
    // storageBucket: "admos-enterprise.appspot.com",
    // messagingSenderId: "342821512757",
    // appId: "1:342821512757:web:c6ac4e1787106ac47af37c",
    // measurementId: "G-GVQM6PNL3W",
};

// app.use(cors({origin: 'https://octo-work.web.app'}));
app.use(cors());
app.use(helmet());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: false
}))

admin.initializeApp(firebaseConfig);
// TEST
// firebase.initializeApp(firebaseConfig);
routes(app);

const port = process.env.PORT || 5000;
server.listen(port, () => {
  console.log('Express server running');
});