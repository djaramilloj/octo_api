const redis = require('redis');
const storeMessages = require('../components/messages/store');

const client = redis.createClient({
    host: process.env.R_host,
    port: process.env.R_port,
    password: process.env.R_password
    // host: process.env.R_host_test,
    // port: process.env.R_port_test
});
const sub = redis.createClient({
    host: process.env.R_host,
    port: process.env.R_port,
    password: process.env.R_password
    // host: process.env.R_host_test,
    // port: process.env.R_port_test
})

client.send_command('config', ['set','notify-keyspace-events','Ex'], SubscribeExpired)

function SubscribeExpired(e,r){
    const expired_subKey = '__keyevent@0__:expired'
    sub.subscribe(expired_subKey,function(){
        sub.on('message', async (chan, number) => {
            // delete chat from firebase if not agent/ticket/finished

            const numbRta = await getShadow(number);
            const splitNumber = numbRta.split(',');
            const splitRta = number.split('_');
            const numberFirebase = splitRta[1].trim();
            const companyId = splitNumber[1].trim();
            await client.del(`${number}_shadow`);
            await storeMessages.deleteChatAfterExpired(companyId, numberFirebase);
        })
    })
}

async function get (number) {
    return new Promise((resolve, reject) =>{
        client.get(`number_${number}`, (err, data) => {
            if(err) return reject(err);
            let res = data || null;
            if (data) {
                res = data;
            }
            resolve(res);
        })
    })
}

async function getShadow (number) {
    return new Promise((resolve, reject) =>{
        client.get(`${number}_shadow`, (err, data) => {
            if(err) return reject(err);
            let res = data || null;
            if (data) {
                res = data;
            }
            resolve(res);
        })
    })
}

async function insert (number) {
    await client.setex(`number_${number}`, 86000, number);
    await client.set(`number_${number}_shadow`, number);
    return true;
}

async function deleteOne (number) {
    await client.del(`number_${number}`);
    await client.del(`number_${number}_shadow`);
    return true;
}

async function update (number, companyId) {
    const numberCheck = await get(number);
    if (numberCheck.includes(companyId)) {
        return true;
    } else {
        await client.append(`number_${number}`, `, ${companyId}`);
        await client.append(`number_${number}_shadow`, `, ${companyId}`)
        return true;
    }
}

async function updateForm (number, companyId, responseId) {
    const numberCheck = await get(number);
    if(!numberCheck) {
        await client.setex(`number_${number}`, 3600, number);
        await client.append(`number_${number}`, `, ${companyId}, form, ${responseId}`)
        return true;
    }
    if (numberCheck.includes('form')) {
        return true;
    } else {
        await client.append(`number_${number}`, `, form, ${responseId}`);
        return true;
    }
}

async function updateNotification (number, companyId, template) {
    const numberCheck = await get(number);
    if(!numberCheck) {
        await client.setex(`number_${number}`, 90000, number);
        await client.append(`number_${number}`, `-${companyId}-notification-${template}`)
        return true;
    }
    if (numberCheck.includes('notification')) {
        return true;
    } else {
        await client.append(`number_${number}`, `-notification-${template}`);
        return true;
    }
}

async function updateOrder (number, companyId, orderId) {
    const numberCheck = await get(number);
    if(!numberCheck) {
        await client.setex(`number_${number}`, 3600, number);
        await client.append(`number_${number}`, `, ${companyId}, order, ${orderId}`)
        return true;
    }
    if (numberCheck.includes('order')) {
        return true;
    } else {
        await client.append(`number_${number}`, `, order, ${orderId}`);
        return true;
    }
}

async function updateOrderTransfer (number, companyId, orderId) {
    const numberCheck = await get(number);
    if(!numberCheck) {
        await client.setex(`number_${number}`, 3600, number);
        await client.append(`number_${number}`, `, ${companyId}, pendingTransfer, ${orderId}`)
        return true;
    }
    if (numberCheck.includes('pendingTransfer')) {
        return true;
    } else {
        await client.append(`number_${number}`, `, pendingTransfer, ${orderId}`);
        return true;
    }
}

async function updateOrderTransferProof (number, companyId, orderId) {
    const numberCheck = await get(number);
    if(!numberCheck) {
        await client.setex(`number_${number}`, 3600, number);
        await client.append(`number_${number}`, `, ${companyId}, TransferProof, ${orderId}`)
        return true;
    }
    if (numberCheck.includes('TransferProof')) {
        return true;
    } else {
        await client.append(`number_${number}`, `, TransferProof, ${orderId}`);
        return true;
    }
}

async function removeForm(number, companyId) {
    await client.setex(`number_${number}`,3600, number);
    await client.append(`number_${number}`, `, ${companyId}`);
    return true;
}

async function remove () {
    client.flushdb();
    return true;
}


module.exports = {
    get,
    insert,
    update,
    remove,
    updateForm,
    removeForm,
    updateNotification,
    updateOrder,
    deleteOne,
    updateOrderTransfer,
    updateOrderTransferProof
}