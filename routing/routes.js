const message = require('../components/messages/routing');
const foreigndb = require('../components/foreigndb/routing');

const routes = (server) => {
    server.use('/message', message);
    server.use('/foreigndb', foreigndb)
}

module.exports = routes;
