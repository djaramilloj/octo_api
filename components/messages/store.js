const firebase = require('firebase-admin');
// const firebase = require('firebase/app')

const setIncomingMessage = async (data) => {
    const timestamp = firebase.firestore.Timestamp.fromDate(new Date);
    // test
    // const timestamp = firebase.firestore.FieldValue.serverTimestamp();
    // set message
    const ref = firebase.firestore().collection('whatsapp')
        .doc(data.companyId)
        .collection('chats')
        .doc(data.number)
    // check if chat already exists
    ref.get().then(async (snapshot) => {
        if(snapshot.exists) {
            // send chat
            const refUpdate = ref.collection('messages');
            const messagePushed = await refUpdate.add({
                message: data.message,
                inbound: data.inbound,
                timestamp: timestamp,
                mediaUrl: (data.mediaUrl) ? data.mediaUrl : '',
                MediaContentType: (data.MediaContentType) ? data.MediaContentType : ''
            });
            await messagePushed.update({
                messageId: messagePushed.id
            })
            // set unseen flag
            await ref.update({
                unseen: true,
                timestamp: timestamp
            })
            // reduce balance of message
            await reduceBalanceMessage(data.companyId);
            return 'message sent'
        } else {
            // create document
            await ref.set({
                number: data.number,
            })
            // send chat
            const refUpdate = ref.collection('messages');
            const messagePushed = await refUpdate.add({
                message: data.message,
                inbound: data.inbound,
                timestamp: timestamp
            });
            await messagePushed.update({
                messageId: messagePushed.id
            })
            // set unseen flag
            await ref.update({
                unseen: true
            })
            // reduce balance of message
            await reduceBalanceMessage(data.companyId);
            return 'message sent'
        }
    }) 
}

async function checkIfChatExists (companyId, number) {
    const ref = firebase.firestore().collection('whatsapp')
        .doc(companyId)
        .collection('chats')
        .doc(number)
    return ref.get();
}

async function createChatRef(companyId, number){
    const ref = firebase.firestore().collection('whatsapp')
        .doc(companyId)
        .collection('chats')
        .doc(number);
    await ref.set({
        number: number,
        finished: false,
        timestamp: firebase.firestore.Timestamp.fromDate(new Date)
    })
}

async function getMainFlowMessage(companyId) {
    const ref = firebase.firestore().collection('whatsapp')
    .doc(companyId)
    .collection('flow')
    .where("main", "==", true)
    return ref.get();
}

async function getFlowMessage(companyId, flowId) {
    const ref = firebase.firestore().collection('whatsapp')
    .doc(companyId)
    .collection('flow')
    .doc(flowId)
    return ref.get();
}

async function deleteCurrentFlow(companyId, number, flowId) {
    const ref = firebase.firestore().collection('whatsapp')
    .doc(companyId)
    .collection('chats')
    .doc(number)
    .collection('flow')
    .doc(flowId)
    return ref.delete();
}

async function deleteNumberWhenNoAgent(companyId, number) {
    const ref = firebase.firestore().collection('whatsapp')
    .doc(companyId)
    .collection('chats')
    .doc(number)

    return ref.delete();
}

async function getFlowMessageOptions(companyId, flowId) {
    const ref = firebase.firestore().collection('whatsapp')
    .doc(companyId)
    .collection('flow')
    .doc(flowId)
    .collection('options')
    .orderBy('optionNumber', 'asc')
    return ref.get();
}


async function saveFlow(companyId, number, flowId, options) {
    const ref = firebase.firestore().collection('whatsapp')
    .doc(companyId)
    .collection('chats')
    .doc(number)
    .collection('flow')
    .doc(flowId)

    return ref.set({
        flowId: flowId,
        options: options
    })
}


async function getFlow(companyId, number){
    const ref = firebase.firestore().collection('whatsapp')
    .doc(companyId)
    .collection('chats')
    .doc(number)
    .collection('flow')

    return ref.get();
}

async function checkIfAgent(companyId, number) {
    const ref = firebase.firestore().collection('whatsapp')
        .doc(companyId)
        .collection('chats')
        .doc(number)

    return ref.get();
}

async function relyOnAgent(companyId, number, assignTo) {
    const ref = firebase.firestore().collection('whatsapp')
        .doc(companyId)
        .collection('chats')
        .doc(number);
    const currentData = await ref.get();
    const newAssignTo = currentData.data().assignTo;
    if (assignTo === 'any') {
        const chosenAgent = await queueAssignment(companyId);
        // const rtaa = await sendEmailReminderChatAgent(chosenAgent.email);
        let rta = [];
        if (newAssignTo && newAssignTo.length !== 0) {
            newAssignTo.forEach(item => {
                if (item.userId !== chosenAgent.userId) {
                    rta.push(item)
                }
            }) 
        }
        if (rta.length !== 0) {
            rta.push({
                email: chosenAgent.email,
                name: chosenAgent.name,
                userId: chosenAgent.userId
            });
        } else {
            rta = [{
                email: chosenAgent.email,
                name: chosenAgent.name,
                userId: chosenAgent.userId
            }]
        }
        return ref.update({
            agent: true,
            assignTo: rta
        });
    } else {
        return ref.update({
            agent: true,
            assignTo: [{
                email: assignTo[0].email,
                name: `${assignTo[0].name} ${assignTo[0].lastname}`,
                userId: assignTo[0].userId
            }]
        }); 
    }
}

async function sendEmailReminderChatAgent(email) {
    const api_url = 'https://us-central1-octo-work.cloudfunctions.net/sendEmailChat';
    const resp = await axios.post(api_url, {email: email});
    return resp;
}

async function queueAssignment(companyId) {
    // automatic agent assignment 
    const ref = firebase.firestore().collection('whatsapp')
        .doc(companyId)
        .collection('weights')
    const rta = await ref.get();
    const resultIteration = queueProcessIteration(rta);
    await increaseChosenAgentWeight(resultIteration, companyId);
    return resultIteration;
}

function queueProcessIteration(input) {
    let arrayOutput = []; 
    let arrayOutputNotAvailable = [];
    let result = null;
    input.forEach(a => {
        if (a.data().available) arrayOutput.push(a.data());
        arrayOutputNotAvailable.push(a.data());
    });
    if (arrayOutput.length >= 1) {
        // iterate over available agents, choose based on weight
        arrayOutput.forEach(i => {
            if (!result) {
                result = i;
            } else {
                if(result.weight > i.weight) {
                    result = i;
                }
            }
        });
    } else {
        // if no agents available, iterate over all agents
        arrayOutputNotAvailable.forEach(i => {
            if (!result) {
                result = i;
            } else {
                if(result.weight > i.weight) {
                    result = i;
                }
            }
        }); 
    }
    return result;
}

async function increaseChosenAgentWeight (user, companyId) {
    const chosenUserRef = firebase.firestore().collection('whatsapp')
        .doc(companyId)
        .collection('weights')
        .doc(user.userId)
    await chosenUserRef.update({
        weight: user.weight + 1
    })
}

async function activateFinishedChat(companyId, number) {
    const timestamp = firebase.firestore.Timestamp.fromDate(new Date);
    // test
    // const timestamp = firebase.firestore.FieldValue.serverTimestamp();
    const ref = firebase.firestore().collection('whatsapp')
        .doc(companyId)
        .collection('chats')
        .doc(number);
    const currentData = await ref.get();
    const assignTo = currentData.data().assignTo;
    // assign chat to new agent leaving the older one
    const newAgent = await queueAssignment(companyId);
    let rta = [];
    assignTo.forEach(item => {
        if (item.userId !== newAgent.userId) {
            rta.push(item)
        }
    })    
    if (rta.length !== 0) {
        rta.push({
            email: newAgent.email,
            name: newAgent.name,
            userId: newAgent.userId
        });
    } else {
        rta = [{
            email: newAgent.email,
            name: newAgent.name,
            userId: newAgent.userId
        }]
    }
    return ref.update({
        finished: false,
        timestamp: timestamp,
        assignTo: rta
    })
}

async function saveOptionClickRecord(companyId, flowId, optionId) {
    const ref = firebase.firestore().collection('whatsapp')
        .doc(companyId)
        .collection('flow')
        .doc(flowId)
        .collection('options')
        .doc(optionId)

    const counterRta = await ref.get();
    return (counterRta.data().counter) ? ref.update({counter: counterRta.data().counter + 1}) : ref.update({counter: 1})
}

async function saveBotResponseData(companyId, number, message) {
    const ref = firebase.firestore().collection('whatsapp')
        .doc(companyId)
        .collection('chats')
        .doc(number)
    
    const rta = await ref.get();
    const responseBot = rta.data().responseBot;
    if(responseBot && responseBot.length >= 1) {
        responseBot.push(message);
        return ref.update({
            responseBot: responseBot
        })
    } else {
        return ref.update({
            responseBot: [message]
        })
    }
}

async function deleteResponseBotData(companyId, number) {
    const ref = firebase.firestore().collection('whatsapp')
        .doc(companyId)
        .collection('chats')
        .doc(number);
    
    return ref.update({
        responseBot: []
    })
}

// async function getTagInfo (companyId) {
//     const ref = firebase.firestore().collection('whatsapp')
//         .doc(companyId)
//         .collection('tags')
//         .doc('training')
        
//     return ref.get();
// }

async function reduceBalanceMessage(companyId) {
    const ref = firebase.firestore().collection('paymentData')
    .doc(companyId)

    const rtaData = await ref.get();
    if (rtaData.data().balance < 1.00) {
        throw new Error('balance deficit')
    } else {
        return await ref.update({
            balance: rtaData.data().balance - rtaData.data().MFee
        })
    } 
}

async function reduceBalanceTemplate(companyId) {
    const ref = firebase.firestore().collection('paymentData')
    .doc(companyId)

    const rtaData = await ref.get();
    if (rtaData.data().balance < 1.00) {
        throw new Error('balance deficit')
    } else {
        return await ref.update({
            balance: rtaData.data().balance - rtaData.data().TFee
        })
    }
}

async function reduceBalanceNotifications (companyId) {
    const ref = firebase.firestore().collection('paymentData')
    .doc(companyId)

    const rtaData = await ref.get();
    if (!rtaData.data().notifications) {
        throw new Error('notifications not allowed');
    } else if (rtaData.data().notifications < 2){
        throw new Error('notifications not available, not enough balance');
    } else{
        await ref.update({
            notifications: rtaData.data().notifications - 1
        })
        return true;
    }
}

async function getBalanceNotifications (companyId) {
    const ref = firebase.firestore().collection('paymentData')
    .doc(companyId)

    const rtaData = await ref.get();
    if (!rtaData.data().notifications) {
        throw new Error('notifications not allowed');
    } else if (rtaData.data().notifications < 2){
        throw new Error('notifications not available, not enough balance');
    } else{
        return true;
    }
}

async function getNotificationApproval(number) {
    const ref = firebase.firestore().collection('notificationsApprovals')
    .doc(number)
    return ref.get();
}

async function setNotificationApproval(number, approval, data) {
    const timestamp = firebase.firestore.Timestamp.fromDate(new Date);
    // test
    // const timestamp = firebase.firestore.FieldValue.serverTimestamp();
    const ref = firebase.firestore().collection('notificationsApprovals')
    .doc(number)
    return ref.set({
        timestamp: timestamp,
        approval: approval,
        number: number
    })
}

async function saveNotificationRecord (messageSid, companyId) {
    const ref = firebase.firestore().collection('notifications')
    .doc(messageSid)

    return ref.set({
        messageSid: messageSid,
        companyId: companyId,
    })
}

async function updateNotificationRecord (messageSid, status) {
    const ref = firebase.firestore().collection('notifications')
    .doc(messageSid)

    const rta = await ref.get();
    const messageInfo = rta.data();
    if(!messageInfo) return 'no notification'
    try {
        if (status === 'delivered') {
            const reduction = await reduceBalanceNotifications(messageInfo.companyId);
            if (reduction) {
                return ref.update({
                    status: status
                })
            }
        } else {
            // this is just an update, notification was already dlivered
            return ref.update({
                status: status
            })
        }
    } catch (error) {
        throw new Error(error);
    }
}

// FORMS LOGIC

async function getMainFormMessage(companyId, formId) {
    let ref = firebase.firestore().collection('whatsapp')
      .doc(companyId)
      .collection('forms')
      .doc(formId)
      .collection('content')
      .where("main", "==", true)
    return ref.get();
  }

async function saveFormFlow(companyId, number, formData) {
    const ref = firebase.firestore().collection('whatsapp')
    .doc(companyId)
    .collection('chats')
    .doc(number)
    .collection('form')
    .doc(formData.questionId)

    return ref.set(formData)
}

async function deleteFormFlow(companyId, number, questionId) {
    const ref = firebase.firestore().collection('whatsapp')
    .doc(companyId)
    .collection('chats')
    .doc(number)
    .collection('form')
    .doc(questionId)

    return ref.delete();
}

async function getCurrentFormFlow(companyId, number) {
    const ref = firebase.firestore().collection('whatsapp')
    .doc(companyId)
    .collection('chats')
    .doc(number)
    .collection('form')

    return ref.get();
}

async function getFormFlow(companyId, formId, questionId) {
    const ref = firebase.firestore().collection('whatsapp')
    .doc(companyId)
    .collection('forms')
    .doc(formId)
    .collection('content')
    .doc(questionId)

    return ref.get();
}

async function saveResponseData(companyId, formId, number, responseId, response, alias) {
    const ref = firebase.firestore().collection('whatsapp')
    .doc(companyId)
    .collection('forms')
    .doc(formId)
    .collection('responses')
    .doc(responseId)
    const data = await ref.get();
    if (data.data()) {
        if (data.data().results) {
            const template = data.data().results;
            template[alias] = response;
            return ref.update({
                results: template
            })
        }
    } else {
        const template = await getFormTemplate(companyId, formId);
        template[alias] = response;
        ref.set({
            number: number,
            responseId: responseId,
            timestamp: firebase.firestore.Timestamp.fromDate(new Date),
            results: template
        })
    }
}

async function getFormTemplate(companyId, formId) {
    const ref = await firebase.firestore().collection('whatsapp')
    .doc(companyId)
    .collection('forms')
    .doc(formId)
    .get()
    return ref.data().template;
}

async function getCompanyInfo(companyId) {
    const ref = await firebase.firestore().collection('company')
    .doc(companyId)
    return ref.get();
}

async function deleteChatAfterExpired(companyId, number) {
    // delete whatsapp chat after redis key expires
    // ref flow
    const refFlow = await firebase.firestore().collection('whatsapp')
    .doc(companyId)
    .collection('chats')
    .doc(number)
    .collection('flow')
    const dataFlow = await refFlow.get();
    if(dataFlow.size > 0) {
        dataFlow.forEach(d => {
            firebase.firestore().collection('whatsapp')
            .doc(companyId)
            .collection('chats')
            .doc(number)
            .collection('flow')
            .doc(d.id)
            .delete();
        })
    }
    // ref form
    const refForm = await firebase.firestore().collection('whatsapp')
    .doc(companyId)
    .collection('chats')
    .doc(number)
    .collection('form')
    const dataForm = await refForm.get();
    if(dataForm.size > 0) {
        dataFlow.forEach(d => {
            firebase.firestore().collection('whatsapp')
            .doc(companyId)
            .collection('chats')
            .doc(number)
            .collection('form')
            .doc(d.id)
            .delete();
        })
    }
    // validation
    const rta = (dataFlow.size === 0 && dataForm.size === 0) ? false : true;
    if(rta) {
        await firebase.firestore().collection('whatsapp')
        .doc(companyId)
        .collection('chats')
        .doc(number)
        .delete();
    };
    return;
}

// ORDER SERVICES

async function sendOrder(companyId, order, setOrderId) {
    const timestamp = firebase.firestore.Timestamp.fromDate(new Date);
    const orderToSend = {...order, timestamp: timestamp};
    if (setOrderId) {
        const ref = firebase.firestore().collection('delivery')
            .doc(companyId)
            .collection('orders')
            .doc(setOrderId);
        await ref.set(orderToSend);
        await ref.update({orderId: setOrderId.substr(setOrderId.length - 4)});
        return;
    } else {
        const ref = firebase.firestore().collection('delivery')
            .doc(companyId)
            .collection('orders');
        const orderRef = await ref.add(orderToSend);
        const orderId = orderRef.id;
        await ref.doc(orderId).update({orderId: orderId.substr(orderId.length - 4)});
        return orderId;
    }
}

async function holdOrder(companyId, order) {
    // hold order while user accepts delivery fee
    // just for transfer generated orders
    const timestamp = firebase.firestore.Timestamp.fromDate(new Date);
    const orderToSend = {...order, timestamp: timestamp};
    const ref = firebase.firestore().collection('delivery')
        .doc(companyId)
        .collection('holdOrder');
    const orderRef = await ref.add(orderToSend);
    const orderId = orderRef.id;
    await ref.doc(orderId).update({orderId: orderId.substr(orderId.length - 4)});
    return orderId;
}

async function deleteHoldOrder(companyId, orderId) {
    const ref = firebase.firestore().collection('delivery')
        .doc(companyId)
        .collection('holdOrder')
        .doc(orderId);
    await ref.delete();
    return;
}

async function getHoldOrder(companyId, orderId) {
    const orderInfo = await firebase.firestore().collection('delivery')
        .doc(companyId)
        .collection('holdOrder')
        .doc(orderId)
        .get();
    return orderInfo.data();
}

async function sendMessageInOrder(data) {
    const timestamp = firebase.firestore.Timestamp.fromDate(new Date);
    const ref = firebase.firestore().collection('delivery')
        .doc(data.companyId)
        .collection('orders')
        .doc(data.orderId);
    // send fading chat
    const messagePushed = await ref.collection('messages').add({
        message: data.message,
        inbound: true,
        timestamp: timestamp,
        mediaUrl: (data.mediaUrl) ? data.mediaUrl : '',
        MediaContentType: (data.MediaContentType) ? data.MediaContentType : ''
    });
    await ref.collection('messages').doc(messagePushed.id).update({
        messageId: messagePushed.id
    })
    // set unseen flag
    await ref.update({
        unseen: true,
    })
    return 'message sent'
}

async function updatePaymentOrder(data) {
    const ref = firebase.firestore().collection('delivery')
    .doc(data.companyId);
    const currentData = await ref.get();
    await ref.update({
        pendingToPay: currentData.data().pendingToPay + data.pendingToPay,
        pendingTransfer: currentData.data().pendingTransfer + data.pendingTransfer
    });
}

async function reduceMonthlyContact(companyId, number) {
    const ref = firebase.firestore().collection('paymentData')
    .doc(companyId);
    await ref.collection('chats').doc(number).set({
        number: number
    });
    const currentData = await ref.get();
    await ref.update({
        monthContacts: currentData.data().monthContacts - 1 
    })
}

async function checkIfNumberHasBeenAccounted(companyId, number) {
    const ref = firebase.firestore().collection('paymentData')
    .doc(companyId)
    .collection('chats')
    .doc(number);
    const resp = await ref.get();
    if (resp.data()) {
        // user has been accounted
        return false;
    } else {
        // user has not been accounted
        return true;
    }
}

async function getCurrentMonthlyContacts(companyId) {
    const ref = firebase.firestore().collection('paymentData')
    .doc(companyId)
    const resp = await ref.get();
    return resp.data().monthContacts;
}

module.exports = {
    setIncomingMessage,
    checkIfChatExists,
    reduceBalanceTemplate,
    reduceBalanceMessage,
    getMainFlowMessage,
    getFlowMessageOptions,
    getFlowMessage,
    saveFlow,
    getFlow,
    deleteCurrentFlow,
    deleteNumberWhenNoAgent,
    checkIfAgent,
    relyOnAgent,
    createChatRef,
    activateFinishedChat,
    saveOptionClickRecord,
    saveBotResponseData,
    deleteResponseBotData,
    reduceBalanceNotifications,
    getBalanceNotifications,
    saveNotificationRecord,
    updateNotificationRecord,
    saveFormFlow,
    deleteFormFlow,
    getCurrentFormFlow,
    getFormFlow,
    getMainFormMessage,
    saveResponseData,
    getNotificationApproval,
    setNotificationApproval,
    getCompanyInfo,
    deleteChatAfterExpired,
    // getTagInfo
    // ORDER SERVICES
    sendOrder,
    sendMessageInOrder,
    holdOrder,
    deleteHoldOrder,
    getHoldOrder,
    updatePaymentOrder,
    reduceMonthlyContact,
    checkIfNumberHasBeenAccounted,
    getCurrentMonthlyContacts
}


