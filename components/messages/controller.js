const responses = require('../../lib/twilioResponses');
const cache = require('../../cache/index');
const companies = require('../../lib/ids');
const store = require('./store');
const codeResponses = require('../../lib/responses');
const {numbersIcon} = require('../../lib/utils');
const { performance, PerformanceObserver } = require('perf_hooks');

const obs = new PerformanceObserver((items) => {

    items.getEntries().forEach((item) => {
        console.log(item.name, + ' ' + item.duration)
    })
})
obs.observe({entryTypes: ['measure']})


const handleIncomingMessage =  async (data) => {
    // cache.remove().then(()=>console.log('removed'));    
    return new Promise( async (resolve, reject) => {
        if (!data.personNumber) {
            return reject('Incomplete information');
        } 
        if (!data.message && !data.mediaUrl) {
            return reject('Incomplete information');
        }
        const incomingMessage = data.message;
        let number = await cache.get(data.personNumber);    
        if (!number) {
            // inserts number without companyId into redis
            await cache.insert(data.personNumber);
            // check if inmediate message matches a company
                // const potentialCompany = incomingMessage.toLowerCase();
                // const arrayCompanies = Object.keys(companies);
                // let company = null;
                // for (let i = 0; i < arrayCompanies.length; i++) {
                //     if (potentialCompany.includes(arrayCompanies[i])) {
                //         company = arrayCompanies[i];
                //         break;
                //     }
                // }
            let company = 'porthos';
            if (company) {
                // assign user to the company
                const getNum = await cache.get(data.personNumber);
                await cache.update(getNum, companies[company]);
                if (incomingMessage.includes('Antes de pagar, espera la confirmación de tu pedido por parte del establecimiento.')) {
                    const orderCreated = await createOrderWithMessage(data.personNumber, companies[company], incomingMessage);
                    resolve(orderCreated);
                } else {
                    // reduce monthly contact
                    const authorizeToAccountUser = await store.checkIfNumberHasBeenAccounted(companies[company], data.personNumber);
                    if (authorizeToAccountUser) await store.reduceMonthlyContact(companies[company], data.personNumber);
                    const authToText = await store.getCurrentMonthlyContacts(companies[company]);
                    if (!authorizeToAccountUser || authToText > 0) {
                        const chatExists = await store.checkIfChatExists(companies[company], getNum);
                        if(chatExists.exists && !chatExists.data().finished && chatExists.data().assignTo) {
                            resolve(`Estás en linea con ${company}, escribe tu inquietud`);
                        }else {
                            const resp = data.message.toLowerCase();
                            if (resp.includes('quiero comunicarme con un agente')) {
                                const getNumUpdated = await cache.get(data.personNumber);
                                const rta = botFlowMessages(data, getNumUpdated, true);
                                resolve(rta); 
                            } else {
                                const getNumUpdated = await cache.get(data.personNumber);
                                const rta = botFlowMessages(data, getNumUpdated, false);
                                resolve(rta);  
                            } 
                        }
                    }
                }
            }else{
                resolve('¿Con cuál empresa deseas comunicarte?');
            }
        } else {  
          if(number.includes('form')) {
            const rta = formFlowContent(data, number);
            resolve(rta); 
          } else if(number.includes('notification')){
            // check if user has accepted notifications
            const resp = incomingMessage.toLowerCase();
            if (resp.includes('si') || resp.includes('yes')) {
                await store.setNotificationApproval(data.personNumber, true);
                const splitNumber = number.split('-');
                const template = splitNumber[3];
                const companyId = splitNumber[1].trim();
                // remove 'notification' from redis
                await cache.deleteOne(data.personNumber);
                const dataSend = {
                    companyId: companyId,
                    template: template,
                    number: data.personNumber
                }
                await sendNot(dataSend);
                resolve('message sent');
            } else {
                await store.setNotificationApproval(data.personNumber, false);
                await cache.deleteOne(data.personNumber);
            }
          } else if (number.includes('order')) {
              console.log(number);
              
            //   this message is sent after order was placed -
            // IT'S VITAL TO CREATE DIFFERENT REDIS FOR EVERY RESTAURANT, AS WELL AS DIFFERENT NUMBER
            // just in case someone orders in two restaurant with Octo at the same time
            //  get order id
            const splitNumber = number.split(',');
            const orderId = splitNumber[3].trim();
            const companyId = splitNumber[1].trim();
            const dataToSend = {
                companyId: companyId,
                orderId: orderId,
                message: data.message,
                mediaUrl: data.mediaUrl,
                MediaContentType: data.MediaContentType
            };
            const rta = await store.sendMessageInOrder(dataToSend);
            resolve(rta);
          } else if (number.includes('pendingTransfer')) {
            const responsePendingOrder = incomingMessage.toLowerCase();
            const splitNumber = number.split(',');
            const orderId = splitNumber[3].trim();
            const companyId = splitNumber[1].trim();
            if (responsePendingOrder === 'si' || responsePendingOrder === '1') {
                // user has accepted the delivery fee, so
                // ask for proof of transfer
                await cache.deleteOne(data.personNumber);
                await cache.updateOrderTransferProof(data.personNumber, companyId, orderId);
                // resolve('Cuenta de ahorros Bancolombia 💵 \n55-12345-99 \nNequi 💵 \n3117608003 \n \n \nPor favor envía UNA CAPTURA DE PANTALLA 📲 que compruebe la realización de la transferencia bancaria para confirmar tu pedido \n_Envía la palabra "CANCELAR" para cancelar el pedido_');
                resolve('Por favor envía UNA CAPTURA DE PANTALLA 📲 que compruebe la realización de la transferencia bancaria para confirmar tu pedido \n_Envía la palabra "CANCELAR" para cancelar el pedido_');

            } else {
                // cancel order and delete redis record
                await cache.deleteOne(data.personNumber);
                await store.deleteHoldOrder(companyId, orderId);
                resolve('No hemos podido generar tu orden con éxito 😔, \nIntenta pidiendo tu orden pagando con efectivo o aceptando el incremento del valor del domicilio pagando con transferencia');
            }
          } else if (number.includes('TransferProof')){
            const responsePendingOrder = incomingMessage.toLowerCase().trim();
            if (responsePendingOrder === 'cancelar') {
                // delete order from hold orders and clean redis
                const splitNumber = number.split(',');
                const orderId = splitNumber[3].trim();
                const companyId = splitNumber[1].trim();
                await cache.deleteOne(data.personNumber);
                await store.deleteHoldOrder(companyId, orderId);
                resolve('Tu pedido ha sido cancelado ❌');
            } else {
                if (data.mediaUrl) { 
                    // user sent picture
                    // allow order to be created
                    const splitNumber = number.split(',');
                    const orderId = splitNumber[3].trim();
                    const companyId = splitNumber[1].trim();
                    const orderInfo = await store.getHoldOrder(companyId, orderId);
                    orderInfo.state = 'transfer-pending';
                    orderInfo.proofTransferPicture = data.mediaUrl;
                    await cache.deleteOne(data.personNumber);
                    await cache.updateOrder(data.personNumber, companyId, orderId);
                    await store.deleteHoldOrder(companyId, orderId);
                    await store.sendOrder(companyId, orderInfo, orderId);
                    // update payment
                    await updatePendingPaymentOrder(companyId, orderInfo.orderCost, true);
                    resolve('¡Hemos recibido tu pedido! No te preocupes, te iremos notificando las actualizaciones de tu pedido 🍕🍔🌮🍣☕');   
                } else {
                    // user didn't send image, ask for image again
                    resolve('Recuerda que debes enviar UNA CAPTURA DE PANTALLA 📲 que compruebe la realización de la transferencia bancaria para confirmar tu pedido \n_Envía la palabra "CANCELAR" para cancelar el pedido_')
                }
            }
          }else {
              if (!number.includes(',')) {
                  const potentialCompany = incomingMessage.toLowerCase();
                  const arrayCompanies = Object.keys(companies);
                  let company = null;
                  for (let i = 0; i < arrayCompanies.length; i++) {
                    if (potentialCompany.includes(arrayCompanies[i])) {
                        company = arrayCompanies[i];
                        break;
                    }
                  }
                  if (company) {
                      cache.update(number, companies[company]);
                      const chatExists = await store.checkIfChatExists(companies[company], number);
                      // reduce monthly contact
                        const authorizeToAccountUser = await store.checkIfNumberHasBeenAccounted(companies[company], data.personNumber);
                        if (authorizeToAccountUser) await store.reduceMonthlyContact(companies[company], data.personNumber);
                        const authToText = await store.getCurrentMonthlyContacts(companies[company]);
                        if (!authorizeToAccountUser || authToText > 0) {
                            if(chatExists.data() && !chatExists.data().finished && chatExists.data().assignTo) {
                                resolve(`Estás en linea con ${company}, escribe tu inquietud`); 
                            }else {
                                const resp = data.message.toLowerCase();
                                if (resp.includes('quiero comunicarme con un agente')) {
                                    const getNumUpdated = await cache.get(data.personNumber);
                                    const rta = botFlowMessages(data, getNumUpdated, true);
                                    resolve(rta); 
                                } else {
                                    const getNumUpdated = await cache.get(data.personNumber);
                                    const rta = botFlowMessages(data, getNumUpdated, false);
                                    resolve(rta);  
                                } 
                            }
                        }
                  } else {
                      resolve('No encontramos la empresa que indicas, recuerda enviar solamente el nombre de la empresa y sin espacios');
                    }
                } else {
                    // reduce monthly contact
                    const authorizeToAccountUser = await store.checkIfNumberHasBeenAccounted(number.split(',')[1].trim(), data.personNumber);
                    if (authorizeToAccountUser) await store.reduceMonthlyContact(number.split(',')[1].trim(), data.personNumber);
                    const authToText = await store.getCurrentMonthlyContacts(number.split(',')[1].trim());
                    if (!authorizeToAccountUser || authToText > 0) {
                        const resp = data.message.toLowerCase();
                        if (resp.includes('quiero comunicarme con un agente')) {
                            const rta = botFlowMessages(data, number, true);
                            resolve(rta);  
                        } else {
                            const rta = botFlowMessages(data, number, false);
                            resolve(rta);   
                        } 
                    }
                }
          }
        }
    })
}

async function createOrderWithMessage (personNumber, companyId, message) {
    let arrayLines = message.split("\n");
    let objOrder = {};
    let commentsOrder = false;
    let order = [];
    arrayLines.forEach(l => {
        if (l.startsWith('-')) {
            order.push(l);
        }
        if (commentsOrder) {
            objOrder.comments = l.trim();
            commentsOrder = false;
        }
        if (l.startsWith('Costo de los productos')) {
            const strClear = l.split(":");
            objOrder.orderCost = strClear[1].trim();
        }
        if (l.startsWith('*Descuento')) {
            const strClear = l.split(":");
            objOrder.discount = strClear[1].trim();
        }
        if (l.startsWith('Costo del empaque')) {
            const strClear = l.split(":");
            objOrder.packageCost = strClear[1].trim();
        }
        if (l.startsWith('*Vuelto de')) {
            const strClear = l.split(":");
            objOrder.cashChange = strClear[1].trim();
        }
        if (l.startsWith('*Tipo de entrega')) {
            const strClear = l.split(":");
            objOrder.deliverMode = strClear[1].trim();
        }
        if (l.startsWith('*Nombre del cliente')) {
            const strClear = l.split(":");
            objOrder.clientName = strClear[1].trim();
        }
        if (l.startsWith('*Teléfono del cliente')) {
            const strClear = l.split(":");
            objOrder.clientPhone = strClear[1].trim();
        }
        if (l.startsWith('*Dirección')) {
            const strClear = l.split(":");
            objOrder.deliveryAddress = strClear[1].trim();
        }
        if (l.includes('https://www.google.com/maps')) {
            objOrder.deliveryPointMap = l;
        }
        if (l.startsWith('_Método de pago')) {
            const strClear = l.split(":");
            objOrder.paymentMethod = strClear[1].replace('_', '').trim();
        }
        if (l.startsWith('*Comentarios acerca')) commentsOrder = true;
    });
    if (!objOrder.cashChange) objOrder.cashChange = "$ 0";              
    if (!objOrder.comments) objOrder.comments = "no hay comentarios"; 
    objOrder.order = order;
    objOrder.whatsappPhone = personNumber;
    if (objOrder.paymentMethod === "_Transferencia_") {
        // bank transfer order - need to confirm
        objOrder.state = 'pending';
        const orderId = await store.holdOrder(companyId, objOrder);
        await cache.deleteOne(personNumber);
        await cache.updateOrderTransferProof(personNumber, companyId, orderId);
        return 'Por favor envía UNA CAPTURA DE PANTALLA 📲 que compruebe la realización de la transferencia bancaria para confirmar tu pedido \n_Envía la palabra "CANCELAR" para cancelar el pedido_';
    } else {
        // cash order
        objOrder.state = 'pending';
        const orderId = await store.sendOrder(companyId, objOrder, null);
        await cache.updateOrder(personNumber, companyId, orderId);
        // update payment
        await updatePendingPaymentOrder(companyId, objOrder.orderCost, false);
        return '¡Hemos recibido tu pedido! No te preocupes, te iremos notificando las actualizaciones de tu pedido 🍕🍔🌮🍣☕';     
    }
}

async function updatePendingPaymentOrder(companyId, orderCost, transfer) {
    const cost = parseInt(orderCost.slice(1).trim().replace(".", ""));
    //  % of Octo may vary, in this case is 5%
    const pendingToPay = cost * 0.05;
    const data = {
        companyId,
        pendingToPay: pendingToPay,
        pendingTransfer: transfer ? 6000 : 0
    }
    await store.updatePaymentOrder(data);
    return;
}

function botFlowMessages(data, number, directMessage) {
    return new Promise(async (resolve, reject) => {
        // send messages to Octo
        const splitNumber = number.split(',');
        const companyId = splitNumber[1].trim();
        let mainData;
        let optionsData = [];
        const dataToPass = {
            companyId: companyId,
            message: data.message,
            inbound: true,
            number: data.personNumber,
            mediaUrl: data.mediaUrl,
            MediaContentType: data.MediaContentType
        }
        const chatExists = await store.checkIfChatExists(dataToPass.companyId, dataToPass.number);
        if(chatExists.exists) {
            // if chat has been closed, it is openes again 
            if(chatExists.data().finished) await store.activateFinishedChat(dataToPass.companyId, dataToPass.number);
            // check if agent has been already assigned
            const conversationStatus = await store.checkIfAgent(dataToPass.companyId, dataToPass.number);
            if(!conversationStatus.data().agent) {
                if (directMessage) {
                    const assignTo = 'any';
                    await store.relyOnAgent(dataToPass.companyId, dataToPass.number, assignTo);
                    if (dataToPass.companyId === 'AOv7ZkVTEpl5JbRQgmiS') {
                        resolve(`Hola, por favor cuéntanos cómo podemos ayudarte y en unos minutos, uno de nuestros agentes te atenderá 😉 . *Horario de atencion: Lunes a domingo de 11 AM A 10 Pm*`)
                    } else {
                        resolve(`Estás en linea con un agente, escribe tu inquietud 😎`)
                    }
                }
                const mainMessage = await store.getMainFlowMessage(dataToPass.companyId);
                if(mainMessage.docs.length !== 0) {
                    const existingFlow = await store.getFlow(dataToPass.companyId, dataToPass.number);
                    if (existingFlow && existingFlow.size !== 0) {
                        // user already saw main message, check response
                        let optionsArray = [];
                        let currentFlow;
                        let responseFound = null;
                        existingFlow.forEach(f => {
                            currentFlow = f.data().flowId;
                            optionsArray = f.data().options;
                        })
                        optionsArray.forEach(o => {
                            if(parseInt(dataToPass.message.trim().toLowerCase()) === o.optionNumber || dataToPass.message.trim().toLowerCase() === o.message) {
                                responseFound = {redirectTo: o.redirectTo, optionId: o.optionId, message: o.message}
                            }
                        })
                        if(!responseFound){
                            resolve('Por favor escribe el numero o la palabra de una de las respuestas disponibles');
                        } else {
                            // save record of clicked option
                            await store.saveBotResponseData(dataToPass.companyId, dataToPass.number, responseFound.message);
                            await store.saveOptionClickRecord(dataToPass.companyId, currentFlow, responseFound.optionId);
                            await store.deleteCurrentFlow(dataToPass.companyId, dataToPass.number, currentFlow);
                            const response = await store.getFlowMessage(dataToPass.companyId, responseFound.redirectTo);
                            // check if current option has more option
                            if(response.data().mainMenuRedirect) {
                                // this option take client to the beginning of the bot
                                const rta = botFlowMessages(data, number, false);
                                resolve(rta);
                            } 
                            else if(response.data().formId) {
                                // this option send a form
                                // delete number from 'chats' node
                                const formInfoRaw = await store.getMainFormMessage(dataToPass.companyId, response.data().formId);
                                let formInfo;
                                formInfoRaw.forEach(f => {  
                                    formInfo = f.data()
                                });
                                formInfo = {...formInfo, formId: response.data().formId};
                                await store.saveFormFlow(dataToPass.companyId, dataToPass.number, formInfo);
                                await cache.deleteOne(dataToPass.number);
                                await cache.updateForm(dataToPass.number, dataToPass.companyId, response.data().responseId);
                                resolve(formInfo.message);
                            }
                            else {
                                if(response.data().options === true) {
                                    let optionsShowData = [];
                                    const options = await store.getFlowMessageOptions(dataToPass.companyId, responseFound.redirectTo);
                                    options.forEach(o => {
                                        const opt = o.data();
                                        optionsData.push(opt);
                                        if (!opt.hidden) optionsShowData.push(opt);
                                    }) 
                                    await store.saveFlow(dataToPass.companyId, dataToPass.number, response.data().flowId, optionsData);
                                    await store.reduceBalanceMessage(dataToPass.companyId);    
                                    if(response.data().mediaUrl) {
                                        await responses.sendMessageMedia(`${response.data().message} \n${optionsShowData.map(m => `${numbersIcon[m.optionNumber.toString()]} ${m.message}`).join('\n')}`, dataToPass.number, response.data().mediaUrl);
                                        resolve('message sent');
                                    } else {
                                        resolve(`${response.data().message} \n${optionsShowData.map(m => `${numbersIcon[m.optionNumber.toString()]} ${m.message}`).join('\n')}`);
                                    }
                                } else {
                                   //  check if conversation has to be rely on a human being
                                   if(response.data().agent){
                                        let assignTo; 
                                        if (response.data().assignTo) {
                                            assignTo = response.data().assignTo;
                                        } else {
                                            assignTo = 'any';
                                        }
                                       await store.relyOnAgent(dataToPass.companyId, dataToPass.number, assignTo);
                                       store.setIncomingMessage(dataToPass)
                                           .then(async(data) => {
                                                await store.reduceBalanceMessage(dataToPass.companyId);
                                               resolve(response.data().message)
                                            //    resolve('Here Octo-Bot says goodbye, one of our human agents will take your inquiry')
    
                                            })
                                           .catch(error => reject(error));
                                    } else {
                                        // delete current responses of bot since no agent will catch the chat
                                        await store.deleteResponseBotData(dataToPass.companyId, dataToPass.number);
                                       // delete number from octo since it is no needed agent help
                                        await store.deleteNumberWhenNoAgent(dataToPass.companyId, dataToPass.number);
                                        // delete number from redis
                                        await cache.deleteOne(dataToPass.number);
                                        // check if response contains a MediaUrl
                                        await store.reduceBalanceMessage(dataToPass.companyId);
                                        if(response.data().mediaUrl) {
                                            await responses.sendMessageMedia(response.data().message, dataToPass.number, response.data().mediaUrl);
                                            resolve('message sent');
                                        } else {
                                            resolve(`${response.data().message}`);
                                        }
                                   }
                                }
                            }
                        }
                    } else {                        
                       // show main message
                       let optionsShowData = [];      
                       mainMessage.forEach(m => {
                           mainData = {
                               message: m.data().message,
                               flowId: m.data().flowId,
                               mediaUrl: m.data().mediaUrl
                           }
                       })
                       const options = await store.getFlowMessageOptions(dataToPass.companyId, mainData.flowId);
                       options.forEach(o => {
                            const opt = o.data();
                            optionsData.push(opt);
                            if (!opt.hidden) optionsShowData.push(opt);
                       }) 
                       await store.saveFlow(dataToPass.companyId, dataToPass.number, mainData.flowId, optionsData);
                       await store.reduceBalanceMessage(dataToPass.companyId);
                    //    resolve(`${mainData.message} \n ${optionsData.map(m => `${m.optionNumber}) ${m.message}`).join('\n')}`) 
                        if(mainData.mediaUrl) {
                            await responses.sendMessageMedia(`${mainData.message} \n${optionsShowData.map(m => `${numbersIcon[m.optionNumber.toString()]} ${m.message}`).join('\n')}`, dataToPass.number, mainData.mediaUrl);
                            resolve('message sent');
                        } else {
                            resolve(`${mainData.message} \n${optionsShowData.map(m => `${numbersIcon[m.optionNumber.toString()]} ${m.message}`).join('\n')}`);
                        }
                    }
                } else {
                    //  send message to human being in Octo
                    await store.relyOnAgent(dataToPass.companyId, dataToPass.number, 'any');
                    store.setIncomingMessage(dataToPass)
                        .then(async(data) => {
                            await store.reduceBalanceMessage(dataToPass.companyId);
                            resolve('Uno de nuestros asesores te responderá en un momento')
                            // resolve('One of our human agents will take your inquiry')
                    })
                    .catch(error => reject(error));
                }
            } else {
               // conversation has been already relied on human / or conversation doesnt exist
            //    const tagInfo = await store.getTagInfo(dataToPass.companyId);
            //     console.log(tagInfo.data());
                if (directMessage) {
                    if (dataToPass.companyId === 'AOv7ZkVTEpl5JbRQgmiS') {
                        resolve(`Hola, por favor cuéntanos cómo podemos ayudarte y en unos minutos, uno de nuestros agentes te atenderá 😉 . *Horario de atencion: Lunes a domingo de 11 AM A 10 Pm*`)
                    } else {
                        resolve(`Estás en linea con un agente, escribe tu inquietud 😎`)
                    }
                } else {
                    store.setIncomingMessage(dataToPass)
                        .then(data => resolve(data))
                        .catch(error => reject(error));
                }
            }
        } else {
            // create chat ref in firebase
            await store.createChatRef(dataToPass.companyId, dataToPass.number);
            if (directMessage) {
                const assignTo = 'any';
                await store.relyOnAgent(dataToPass.companyId, dataToPass.number, assignTo);
                if (dataToPass.companyId === 'AOv7ZkVTEpl5JbRQgmiS') {
                    resolve(`Hola, por favor cuéntanos cómo podemos ayudarte y en unos minutos, uno de nuestros agentes te atenderá 😉 . *Horario de atencion: Lunes a domingo de 11 AM A 10 Pm*`)
                } else {
                    resolve(`Estás en linea con un agente, escribe tu inquietud 😎`)
                }
            }
            // get flow if exists
            const mainMessage = await store.getMainFlowMessage(dataToPass.companyId);
            if(mainMessage.docs.length !== 0) {
                const existingFlow = await store.getFlow(dataToPass.companyId, dataToPass.number);
                if (existingFlow && existingFlow.size !== 0) {
                    // user already saw main message, check response
                    let optionsArray = [];
                    let currentFlow;
                    let responseFound = null;
                    existingFlow.forEach(f => {
                        currentFlow = f.data().flowId;
                        optionsArray = f.data().options;
                    })
                    optionsArray.forEach(o => {
                        if(parseInt(dataToPass.message.trim().toLowerCase()) === o.optionNumber || dataToPass.message.trim().toLowerCase() === o.message) {
                            responseFound = {redirectTo: o.redirectTo, optionId: o.optionId, message: o.message}
                        }
                    })
                    if(!responseFound){
                        resolve('Por favor escribe el numero o la palabra de una de las respuestas disponibles')
                    } else {
                        // save record of clicked option
                        await store.saveBotResponseData(dataToPass.companyId, dataToPass.number, responseFound.message);
                        await store.saveOptionClickRecord(dataToPass.companyId, currentFlow, responseFound.optionId);
                        await store.deleteCurrentFlow(dataToPass.companyId, dataToPass.number, currentFlow);
                        const response = await store.getFlowMessage(dataToPass.companyId, responseFound.redirectTo);
                        if (response.data().mainMenuRedirect) {                            
                            // this option takes the user to the beginning of the bot
                            const rta = botFlowMessages(data, number, false);
                            resolve(rta);
                        } 
                        else if(response.data().formId) {
                            // this option send a form
                            // delete number from 'chats' node
                            const formInfoRaw = await store.getMainFormMessage(dataToPass.companyId, response.data().formId);
                            let formInfo;
                            formInfoRaw.forEach(f => {
                                
                                
                                formInfo = f.data()
                            });
                            formInfo = {...formInfo, formId: response.data().formId};
                            await store.saveFormFlow(dataToPass.companyId, dataToPass.number, formInfo);
                            await cache.deleteOne(dataToPass.number);
                            await cache.updateForm(dataToPass.number, dataToPass.companyId, response.data().responseId);
                            resolve(formInfo.message);
                        }
                        else {
                            // check if current option has more option
                            if(response.data().options === true) {
                                let optionsShowData = [];
                                const options = await store.getFlowMessageOptions(dataToPass.companyId, responseFound);
                                options.forEach(o => {
                                    const opt = o.data();
                                    optionsData.push(opt);
                                    if (!opt.hidden)  optionsShowData.push(opt);
                                }) 
                                await store.saveFlow(dataToPass.companyId, dataToPass.number, response.data().flowId, optionsData);
                                await store.reduceBalanceMessage(dataToPass.companyId);
    
                                if(response.data().mediaUrl) {
                                    await responses.sendMessageMedia(`${response.data().message} \n${optionsShowData.map(m => `${numbersIcon[m.optionNumber.toString()]} ${m.message}`).join('\n')}`, dataToPass.number, response.data().mediaUrl);
                                    // (response.data().message) ? resolve(`${response.data().message} \n ${optionsShowData.map(m => `${m.optionNumber}) ${m.message}`).join('\n')}`) :'message sent';
                                    resolve('message sent');
                                } else {
                                    resolve(`${response.data().message} \n${optionsShowData.map(m => `${numbersIcon[m.optionNumber.toString()]} ${m.message}`).join('\n')}`);
                                }                            
                            } else {
                                //  check if conversation has to be rely on a human being
                                if(response.data().agent){
                                    let assignTo; 
                                    if (response.data().assignTo) {
                                        assignTo = response.data().assignTo;
                                    } else {
                                        assignTo = 'any';
                                    }
                                    await store.relyOnAgent(dataToPass.companyId, dataToPass.number, assignTo);
                                    // const tagInfo = await store.getTagInfo(dataToPass.companyId);
                                    // console.log(tagInfo.data());
                                    store.setIncomingMessage(dataToPass)
                                        .then(async(data) => {
                                            await store.reduceBalanceMessage(dataToPass.companyId);
                                            resolve(response.data().message)
                                            // resolve('Here Octo-Bot says goodbye, one of our human agents will take your inquiry')
                                        })
                                        .catch(error => reject(error));
                                } else {
                                    // delete current responses of bot since no agent will catch the chat
                                    await store.deleteResponseBotData(dataToPass.companyId, dataToPass.number);
                                    // delete number from octo since it is no needed agent help
                                    await store.deleteNumberWhenNoAgent(dataToPass.companyId, dataToPass.number);
                                    // delete number from redis
                                    await cache.deleteOne(dataToPass.number);
                                    // check if response contains a MediaUrl
                                    await store.reduceBalanceMessage(dataToPass.companyId);
                                    if(response.data().mediaUrl) {
                                        await responses.sendMessageMedia(response.data().message, dataToPass.number, response.data().mediaUrl);
                                        resolve('message sent');
                                    } else {
                                        resolve(`${response.data().message}`);
                                    }
                                }
                            }
                        }
                    }
                } else {
                    // show main message
                    let optionsShowData = [];      
                    mainMessage.forEach(m => {
                        mainData = {
                            message: m.data().message,
                            flowId: m.data().flowId,
                            mediaUrl: m.data().mediaUrl
                        }
                    })
                    const options = await store.getFlowMessageOptions(dataToPass.companyId, mainData.flowId);
                    options.forEach(o => {
                        const opt = o.data();
                        optionsData.push(opt);
                        if (!opt.hidden) optionsShowData.push(opt);
                    }) 
                    await store.saveFlow(dataToPass.companyId, dataToPass.number, mainData.flowId, optionsData);
                    await store.reduceBalanceMessage(dataToPass.companyId);
                    // resolve(`${mainData.message} \n ${optionsData.map(m => `${m.optionNumber}) ${m.message}`).join('\n')}`) 
                    if(mainData.mediaUrl) {
                        await responses.sendMessageMedia(`${mainData.message} \n${optionsShowData.map(m => `${numbersIcon[m.optionNumber.toString()]} ${m.message}`).join('\n')}`, dataToPass.number, mainData.mediaUrl);
                        resolve('message sent');
                    } else {
                        resolve(`${mainData.message} \n${optionsShowData.map(m => `${numbersIcon[m.optionNumber.toString()]} ${m.message}`).join('\n')}`);
                    }
                }
            } else {
                //  send message to human being in Octo
                await store.relyOnAgent(dataToPass.companyId, dataToPass.number, 'any');
                // const tagInfo = await store.getTagInfo(dataToPass.companyId);
                // console.log(tagInfo.data());
                store.setIncomingMessage(dataToPass)
                    .then(async(data) => {
                        await store.reduceBalanceMessage(dataToPass.companyId);
                        resolve('Uno de nuestros asesores te responderá en un momento')
                        // resolve('One of our human agents will take your inquiry')
                    })
                    .catch(error => reject(error));
            }
        }
    })
}

function formFlowContent(data, number) {
    return new Promise(async(resolve, reject) => {
        const splitNumber = number.split(',');
        const companyId = splitNumber[1].trim();
        const responseId = splitNumber[3].trim();
        try {
            await store.reduceBalanceMessage(companyId);
            let currentForm;
            // get current form flow
            const currentFlowForm = await store.getCurrentFormFlow(companyId, data.personNumber);
            currentFlowForm.forEach(f => {
                currentForm = f.data();
            })
            // save response data
            let messageRta;
            let numberRta = parseFloat(data.message.trim());
            if (currentForm.responseType === 'number') {
                if (!numberRta) {
                    resolve('Esta respuesta debe ser un número');
                } else {
                    if (numberRta > currentForm.maxNum || numberRta < currentForm.minNum) {
                        resolve('Por favor selecciona un número que este dentro del rango disponible');
                    } else {
                        messageRta = parseFloat(data.message.trim());
                        await store.saveResponseData(companyId, currentForm.formId, data.personNumber, responseId, messageRta, currentForm.alias);
                        // get next form flow
                        const nextFormRta = await store.getFormFlow(companyId, currentForm.formId, currentForm.next);
                        const nextFormFlow = {
                            ...nextFormRta.data(),
                            formId: currentForm.formId
                        }
                        // delete current flow
                        await store.deleteFormFlow(companyId, data.personNumber, currentForm.questionId);
                        // save next flow as current flow
                        if (!nextFormFlow.end) {
                            // save current response
                            await store.saveFormFlow(companyId, data.personNumber, nextFormFlow);
                        } else {
                            // delete 'form' from redis 
                            await cache.removeForm(data.personNumber, companyId);
                        }
                        // return message of next flow
                        resolve(nextFormFlow.message);
                    }
                }
            } else if (currentForm.responseType === 'string') {
                if (typeof(data.message) !== 'string') {
                    resolve('Esta respuesta debe ser una palabra o texto');
                } else {
                    messageRta = data.message;                    
                    await store.saveResponseData(companyId, currentForm.formId, data.personNumber, responseId, messageRta, currentForm.alias);
                    // get next form flow
                    const nextFormRta = await store.getFormFlow(companyId, currentForm.formId, currentForm.next);
                    const nextFormFlow = {
                        ...nextFormRta.data(),
                        formId: currentForm.formId
                    }
                    // delete current flow
                    await store.deleteFormFlow(companyId, data.personNumber, currentForm.questionId);
                    // save next flow as current flow
                    if (!nextFormFlow.end) {
                        // save current response
                        await store.saveFormFlow(companyId, data.personNumber, nextFormFlow);
                    } else {
                        // delete 'form' from redis 
                        await cache.removeForm(data.personNumber, companyId);
                    }
                    // return message of next flow
                    resolve(nextFormFlow.message);
                }
            } else if (currentForm.responseType === 'name') {
                messageRta = data.message;                    
                await store.saveResponseData(companyId, currentForm.formId, data.personNumber, responseId, messageRta, currentForm.alias);
                // get next form flow
                const nextFormRta = await store.getFormFlow(companyId, currentForm.formId, currentForm.next);
                const messagePlacehold = nextFormRta.data().message.replace('{{name}}', messageRta);
                const nextFormFlow = {
                    ...nextFormRta.data(),
                    message: messagePlacehold ,
                    formId: currentForm.formId
                }
                // delete current flow
                await store.deleteFormFlow(companyId, data.personNumber, currentForm.questionId);
                // save next flow as current flow
                if (!nextFormFlow.end) {
                    // save current response
                    await store.saveFormFlow(companyId, data.personNumber, nextFormFlow);
                } else {
                    // delete 'form' from redis 
                    await cache.removeForm(data.personNumber, companyId);
                }
                // return message of next flow
                resolve(nextFormFlow.message);
            }
        } catch(error) {
            console.error(error);          
            reject('internal server error')
        }
    })
}

const handleOutboundMessage = (data) => {
    performance.mark('A');
    return new Promise(async(resolve, reject) => {
        if (
            // !data.message 
            !data.number 
            || data.template === undefined 
            || data.template === null 
            || !data.companyId) {
            return reject('Incomplete information');
        }
        try {   
            if (data.template === true) {
                await store.reduceBalanceTemplate(data.companyId);
                performance.mark('B');
                performance.measure('BALANCE REDUCE', 'A', 'B');
            } else {
                await store.reduceBalanceMessage(data.companyId);
                performance.mark('B');
                performance.measure('BALANCE REDUCE', 'A', 'B');
            }
            if (data.form) {
                await cache.updateForm(data.number, data.companyId, data.responseId);
                performance.mark('C');
                performance.measure('FORM CACHE UPDATING', 'A', 'C');
            }
            if (data.mediaUrl) {
                let messageVn = data.message;
                if(!data.message) messageVn = 'nota de voz' 
                responses.sendMessageMedia(messageVn, data.number, data.mediaUrl)
                    .then((dataRta) => {
                        performance.mark('D');
                        performance.measure('MEDIA SENDING', 'A', 'D');
                        return resolve({rta: dataRta, mediaUrl: data.mediaUrl});
                    })
                    .catch(error => {
                        return reject(error);
                    }) 
            } else {
                responses.sendMessage(data.message, data.number)
                    .then((dataRta) => {
                        performance.mark('E');
                        performance.measure('REGULAR MESSAGE SENDING', 'A', 'E');
                        return resolve({rta: dataRta, mediaUrl: null});
                    })
                    .catch(error => {
                        return reject(error);
                    })    
            }
        } catch(error) {
            // console.error(error);
            return reject('saldo insuficiente');
        }
    })
}


const checkUserInRedis = (data) => {
    // check if user exists in redis to define wether or not send template
    return new Promise(async (resolve, reject) => {
        if (!data.companyId || !data.personNumber) {
            return reject('Incomplete information');
        }
        let number = await cache.get(data.personNumber);
        if(!number) {
            return resolve('false');
        } else {
            const companyId = data.companyId.trim();
            if (!number.includes(companyId)) {
                return resolve('false');
            } else {
                return resolve('true');
            }
        }
    })
}

const sendNotification = (data) => {
    return new Promise(async (resolve, reject) => {
        if (!data.number || !data.template) {
            const rta = codeResponses.error('Incomplete information')
            reject(rta);
        }
        try {  
            // check if user has approved notifications sends
            const approval = await store.getNotificationApproval(data.number);
            const companyInfo = await store.getCompanyInfo(data.companyId);
            if (!approval.data()) { 
                await cache.updateNotification(data.number, data.companyId, data.template);
                await store.reduceBalanceNotifications(data.companyId);
                await responses.sendMessage(
                    `!Hola! somos ${companyInfo.data().name}, tenemos un mensaje importante para ti, ¿Deseas recibir notificaciones mediante WhatsApp acerca de nuestro producto/servicio? Responde 'SI' para aceptar y 'NO' para rechazar`, 
                    data.number)
                const rta = codeResponses.optIn();
                resolve(rta);
            } else {                
                if(approval.data().approval) {
                    const rta = await sendNot(data);  
                    resolve(rta);   
                } else {
                    const rta = codeResponses.error('User has denied notifications receival');
                    reject(rta); 
                }
            }
        } catch(error) {
            if (error.message === 'notifications not allowed' || error.message === 'notifications not available, not enough balance') {
                const rta = codeResponses.error('Notifications not allowed or insuficient notification balance');
                reject(rta);
            } else {
                console.log(error);
                
                const rta = codeResponses.error('error sending the notification, contact administrator')
                reject(rta);
            }
        }
    })
}

async function sendNot(data) {  
    await store.getBalanceNotifications(data.companyId);
    const messageSid = await responses.sendMessage(
        data.template, 
        data.number)
    await store.saveNotificationRecord(messageSid, data.companyId);
    const rta = codeResponses.success();
    return rta;
}

const updateNotificationStatus = async (data) => {
    try {
        if(data.event === 'READ') {
            await store.updateNotificationRecord(data.messageSid, 'read');
            return 'notification read';
        } else if(data.event === 'DELIVERED') {
            await store.updateNotificationRecord(data.messageSid, 'delivered');
            return 'notification delivered'
        }
        return;
    } catch(error) {
        console.error(error);
        const rta = codeResponses.error('error sending the notification, contact administrator')
        return rta;
    }
}

const sendOrderStatus = (data) => {
    return new Promise(async (resolve, reject) => {
        if ( 
            !data.number 
            || !data.message 
            || !data.companyId)
        {
            return reject('Incomplete information');
        }
        try {
            const sendMessage = await responses.sendMessage(data.message, data.number);
            if (data.endOrder) await cache.deleteOne(data.number);
            return resolve(`update sent with MessageSid: ${sendMessage}`);
        } catch(error) {
            console.log(error);
            return reject('Internal server error')
        }
    })
}

module.exports = {
    handleIncomingMessage,
    handleOutboundMessage,
    checkUserInRedis,
    sendNotification,
    updateNotificationStatus,
    sendOrderStatus
}