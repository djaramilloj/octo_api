const express = require('express');
const Router = express.Router();
const MessagingResponse = require('twilio').twiml.MessagingResponse;
const controller = require('./controller');
const { performance, PerformanceObserver } = require('perf_hooks');

const obs = new PerformanceObserver((items) => {

    items.getEntries().forEach((item) => {
        console.log(item.name, + ' ' + item.duration)
    })
})
obs.observe({entryTypes: ['measure']})

Router.post('/', async (req, res) => {
    performance.mark('A');
    const data = {
        personNumber: req.body.From,
        message: req.body.Body,
        mediaUrl: (req.body.MediaUrl0) ? req.body.MediaUrl0 : null,
        MediaContentType: (req.body.MediaContentType0) ? req.body.MediaContentType0 : null
    }      
    controller.handleIncomingMessage(data)
    .then((data) => {
        performance.mark('B');
        performance.measure('message entry', 'A', 'B');
        if(data !== 'message sent') {
            const twiml = new MessagingResponse();
            res.writeHead(200, {'Content-Type': 'text/xml'});
            twiml.message(data);
            res.end(twiml.toString());
        }
    })
    .catch(error => {
        performance.mark('B');
        performance.measure('message entry', 'A', 'B');
        console.error(error);
    })
  });
  
  Router.post('/check-user', (req, res) => {
    // consumed by Octo   
    const data = {
        companyId: req.body.companyId,
        personNumber: req.body.number,
    }
    console.log(data)
    controller.checkUserInRedis(data)
        .then(data => {
            console.log(data)
            res.status(200).send(data);
        })
        .catch(error => {
            console.error(error);
            res.status(500).send('Internal Server Error');
        })
});


  Router.post('/sendFromOcto', (req, res) => {
        performance.mark('A');
        // consumed by Octo   
        const data = {
            template: req.body.template, 
            message: req.body.message,
            number: req.body.number,
            companyId: req.body.companyId,
            mediaUrl: (req.body.mediaUrl) ? req.body.mediaUrl : null,
            form: (req.body.form) ? req.body.form : null,
            responseId: req.body.responseId
        }   
        controller.handleOutboundMessage(data)
            .then(data => {
                performance.mark('B');
                performance.measure('message output', 'A', 'B');
                res.status(201).send({message:'Message Sent', mediaUrl: data.mediaUrl});
            })
            .catch(error => {
                console.error(error);
                if (error === 'saldo insuficiente') {
                    res.status(400).send(error);
                }else {
                    res.status(500).send('Internal Server Error');
                }
            })
  });

  Router.post('/sendNotification', async (req, res) => {
    // consumed by Third parties 
    const data = {
        template: req.body.template,
        number: req.body.number,
        companyId: req.body.apiKey
    }      
    controller.sendNotification(data)
        .then(dataRta => {
            res.send(dataRta);   
        })
        .catch(e => {
            res.send(e);
    })
});

Router.post('/updateNotificationStatus', async (req, res) => {
    // consumed by Twilio
    const data = {
        messageSid: req.body.MessageSid,
        event: req.body.EventType
    }
    controller.updateNotificationStatus(data)
        .then((rta) => {
            if (rta !== 'notification read') {
                res.send(rta);
            }
        })
        .catch(error => {
            res.send(error);
        })
});

Router.post('/orderStatusNotification', async (req, res) => {
    const data = {
        message: req.body.message,
        number: req.body.number,
        companyId: req.body.companyId,
        endOrder: req.body.endOrder ? req.body.endOrder : false
    }
    controller.sendOrderStatus(data)
        .then(data => {
            res.status(201).send(data);
        })
        .catch(error => {
            if (error === 'Incomplete information') {
                res.status(400).send(error);
            }else {
                res.status(500).send('Internal Server Error');
            }
        })
})


module.exports = Router;