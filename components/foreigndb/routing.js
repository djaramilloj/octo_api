const express = require('express');
const Router = express.Router();
const DBConnection = require('../../lib/mysql');

Router.post('/encuesta', async (req, res) => {
    const begin = req.body.begin;
    const end = req.body.end;
    const after = req.body.after;
    const before = req.body.before;
    try {
        const mysql = new DBConnection();
        const currentIndex = after ? after : 0;
        const rta = await mysql.getSurveys(begin, end, currentIndex);
        const response = {
            total: rta.length,
            data: rta,
            currentIndex: currentIndex
        };
        res.send(response)
    } catch(error) {
        console.log('there was an error bringing data: ', error);
        res.send('ERROR')
    }
})

module.exports = Router;