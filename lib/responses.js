function optIn() {
    const rta = {
        message: 'opt-in has been sent',
        code: 'S-001'
    }
    return rta;
}

function success() {
    const rta = {
        message: 'notification has been sent',
        code: 'S-002'
    }
    return rta;
}

function error(message) {
    const rta = {
        message: message,
        code: 'E-001' 
    } 
    return rta;
}

module.exports = {
    success,
    error,
    optIn
}