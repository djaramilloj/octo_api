const accountSid = process.env.T_accountSid;
const authToken = process.env.T_authToken;
const client = require('twilio')(accountSid, authToken);

async function sendMessage (body, number) {
    try {
        const messageSent = await client.messages
            .create({
                from: process.env.T_phone,
                // from: process.env.T_phone_test,
                body: body,
                to: `${number}`
            })
        return messageSent.sid;
    } catch (error) {
        return error;
    }
}

async function sendMessageMedia (body, number, mediaUrl) {
    try {
        const messageSent = await client.messages
            .create({
                from: process.env.T_phone,
                // from: process.env.T_phone_test,
                body: body,
                mediaUrl: [`${mediaUrl}`],
                to: `${number}`
            })
        return messageSent.accountSid;
    } catch (error) {
        return error;
    }
}

module.exports = {sendMessage, sendMessageMedia};