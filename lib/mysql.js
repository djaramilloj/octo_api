const mysql = require('mysql');

const dbconfig = {
  host: process.env.porthos_mysql_host,
  user: process.env.porthos_mysql,
  password: process.env.porthos_mysql,
  database: process.env.porthos_mysql
};

class DBConnection {
    
    _connection
    
    constructor() {
        this.handleCon();
    }

    handleCon() {
        this._connection  = mysql.createPool({
            connectionLimit: 10,
            host: dbconfig.host,
            user: dbconfig.user,
            password: dbconfig.password,
            database: dbconfig.database
        });

        this._connection.on('error', function(err) {
            console.log('MYSQL ERROR', err);
        });
    }

    getSurveys(begin, end, after, before) {
        return new Promise((resolve, reject) => this._connection.query(`
            SELECT * FROM encuesta n
            WHERE calificacion < 6
            AND fecha <= '${end}'
            AND fecha >= '${begin}'
            AND id > ${after}
            ORDER BY id
            LIMIT 100;
        `, (err, rows) => {
            if (err) reject(err);
            resolve(rows);
        }))
    }
}

module.exports = DBConnection;